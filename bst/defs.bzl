"""BuildStream Rules

Starlark rules for running [BuildStream](https://gitlab.com/BuildStream/buildstream)
on BuildStream projects as targets of Bazel projects.
"""

def _execute_bst(repository_ctx, args, log_path):
    """Compose the argument to bst and execute in the project directory."""

    # check for bst binary
    bst_bin = repository_ctx.path(repository_ctx.attr.bst_bin) if repository_ctx.attr.bst_bin else repository_ctx.which("bst")

    if not bst_bin:
        fail("'bst' was not found on your $PATH")
    if not bst_bin.exists:
        fail("Buildstream binary was not found: '{}'".format(bst_bin))

    # check for the project directory
    # repository_ctx.path() cannot resolve labels that are directories
    # see https://github.com/bazelbuild/bazel/issues/3901
    project_dir = repository_ctx.path(repository_ctx.attr.project_dir) if repository_ctx.attr.project_dir else repository_ctx.path(repository_ctx.os.environ.get("PWD"))
    if not project_dir.exists:
        fail("Project directory was not found: {}".format(project_dir))

    bst_cmd = ["{}".format(bst_bin)] + _get_bst_common_options() + _get_bst_options(repository_ctx) + args

    repository_ctx.report_progress("Executing: {}".format(bst_cmd))
    res = repository_ctx.execute(bst_cmd, quiet = repository_ctx.attr.quiet, working_directory = "{}".format(project_dir), timeout = repository_ctx.attr.timeout)
    repository_ctx.report_progress("Executed: {}".format(bst_cmd))

    # send the logs
    export_err = log_path + "_stderr.log"
    export_out = log_path + "_stdout.log"

    repository_ctx.file(export_err, content = res.stderr, executable = False)
    repository_ctx.report_progress("Wrote: {}".format(export_err))
    repository_ctx.file(export_out, content = res.stdout, executable = False)
    repository_ctx.report_progress("Wrote: {}".format(export_out))

    if res.return_code != 0:
        trimmed = repository_ctx.attr.trimmed
        msg = "Process failed with code {}.\n".format(res.return_code)

        if trimmed == -1:
            fail(msg + "{}".format(res.stderr))
        else:
            log = "\n".join(res.stderr.splitlines()[-trimmed:])
            fail(msg + "Printing the last {} lines:\n{}".format((trimmed - 1), log))

    exports = [export_out, export_err]
    return exports

def _get_bst_common_options():
    """Return some common CLI options for running bst"""
    return [
        "--on-error",
        "terminate",
        "--no-interactive",
    ]

def _get_bst_options(repository_ctx):
    """Compose the bst options from the configuration dictionary."""
    bst_options = []
    for k, v in repository_ctx.attr.config.items():
        bst_options.extend([
            "-o",
            "{}".format(k),
            "{}".format(v),
        ])
    return bst_options

def _bst_build(repository_ctx):
    """Compose the specific build subcommand and execute bst."""
    build_cmd = [
        "build",
        "{}".format(repository_ctx.attr.element),
    ]
    log_path = repository_ctx.name + "_build"
    return _execute_bst(repository_ctx, build_cmd, log_path)

def _bst_checkout(repository_ctx):
    """Checkout the artifact from the build."""
    target = repository_ctx.name
    if repository_ctx.attr.checkout_dir:
        target = repository_ctx.attr.checkout_dir

    # checkout directory is relative to the bst project directory
    # so first resolve relative to the external repo
    checkout = "{}/{}".format(repository_ctx.path("."), target)
    checkout_cmd = [
        "artifact",
        "checkout",
        "--force",
        "--directory",
        "{}".format(checkout),
    ]

    # add dependencies
    if repository_ctx.attr.deps:
        checkout_cmd.extend([
            "--deps",
            "{}".format(repository_ctx.attr.deps),
        ])

    checkout_cmd.append("{}".format(repository_ctx.attr.element))

    # checkout and export the logs
    log_path = target + "_checkout"
    exports = _execute_bst(repository_ctx, checkout_cmd, log_path)
    return exports, checkout

def _bst_element_impl(repository_ctx):
    """Implementation of the `bst_element` rule."""
    repository_ctx.report_progress("Building '{}' which targets element '{}'".format(repository_ctx.name, repository_ctx.attr.element))

    if repository_ctx.attr.build_file and repository_ctx.attr.build_file_content:
        fail("Specify only one of `build_file` or `build_file_content`.")

    # bst build
    exports = _bst_build(repository_ctx)
    _exports, checkout = _bst_checkout(repository_ctx)
    exports.extend(_exports)

    content_lines = [
        'package(default_visibility = ["//visibility:public"])',
        "exports_files({})".format(exports),
    ]

    # the checkout is constrained by "@<name>//<name>:<target>"
    repository_ctx.file("BUILD.bazel", content = "\n".join(content_lines))

    # override/create the subpackage if necessary
    subpackage_content = ""
    if repository_ctx.attr.build_file:
        subpackage_content = repository_ctx.read(repository_ctx.attr.build_file)
    if repository_ctx.attr.build_file_content:
        subpackage_content = repository_ctx.attr.build_file_content
    if subpackage_content:
        repository_ctx.file(checkout + "/BUILD.bazel", content = subpackage_content)

bst_element = repository_rule(
    local = True,  # required for always allowing bst to manage changes
    configure = True,  # will need to check for the path to tools
    implementation = _bst_element_impl,
    attrs = {
        "element": attr.string(mandatory = True, doc = "attr.string: element filename"),
        "project_dir": attr.string(mandatory = False, doc = "attr.string: absolute path to bst project"),
        "bst_bin": attr.string(mandatory = False, doc = "attr.string: absolute path to bst binary"),
        "config": attr.string_dict(mandatory = False, default = {}, doc = "attr.string_dict: bst configuration options `-o key value`"),
        "timeout": attr.int(mandatory = False, default = 600, doc = "attr.int: timeout for execution"),
        "trimmed": attr.int(mandatory = False, default = -1, doc = "attr.int: truncate stderr"),
        "quiet": attr.bool(mandatory = False, default = True, doc = "attr.bool: supress output from bst"),
        "build_file": attr.label(allow_single_file = True, mandatory = False, doc = "attr.string: packaging for the bst artifact"),
        "build_file_content": attr.string(mandatory = False, doc = "attr.string: packaging for the bst artifact"),
        "checkout_dir": attr.string(mandatory = False, doc = "attr.string: directory relative to the external repository to checkout the bst artifact to"),
        "deps": attr.string(mandatory = False, doc = "attr.string: element dependencies to checkout with the artifacts."),
    },
)
"""Runs BuildStream against a given element.

This rule runs [BuildStream](https://gitlab.com/BuildStream/buildstream) against an element declared in a BuildStream project.

Args:
  element: *mandatory* name of the element file
  project_dir: absolute path containing the buildstream project config (default is $PWD)
  bst_bin: absolute path to the Buildstream binary (default is 'bst' found on $PATH)
  config: a dictionary of `-o` options to append to `bst` at execution in the form `-o <key> <value>` (default is {})
  timeout: time in seconds to wait for bst executions before failing (default is 600)
  trimmed: truncate the stderr resulting from a failed execution to this many lines (default is -1 meaning all lines)
  quiet: boolean indicating whether to suppress output from bst on stdout/stderr (default is True)
  build_file: optional file which will replace the packaging of the bst artifact (default is none)
  build_file_content: optional string content which will replace the packaging of the bst artifact (default is non)
  checkout_dir: optional path relative to the external repository to checkout the bst artifact to (default is the name of the rule)
  deps: optional string representing what set of element dependencies to checkout with the artifact (default is to use BuildStream default)

"""
